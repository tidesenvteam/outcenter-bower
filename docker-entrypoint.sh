#!/usr/bin/env bash

isCommand() {
  for cmd in \
    "cache" \
    "help" \
    "home" \
    "info" \
    "init" \
    "install" \
    "link" \
    "list" \
    "login" \
    "lookup" \
    "prune" \
    "register" \
    "search" \
    "update" \
    "uninstall" \
    "unregister" \
    "version" \
    "bower"
  do
    if [ -z "${cmd#"$1"}" ]; then
      return 0
    fi
  done

  return 1
}

# check if the first argument passed in looks like a flag
if [ "$(printf %c "$1")" = '-' ]; then
  set -- /sbin/tini -- bower "$@"
# check if the first argument passed in is bower
elif [ "$1" = 'bower' ]; then
  set -- /sbin/tini -- "$@"
# check if the first argument passed in matches a known command
elif isCommand "$1"; then
  set -- /sbin/tini -- bower "$@"
fi

exec "$@"